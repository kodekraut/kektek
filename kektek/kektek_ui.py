import os
import sys

import PySide2.QtCore as _core
import PySide2.QtWidgets as _widgets
import PySide2.QtUiTools as _uitools

import kektek.scanner as _scanner
import kektek.clipper as _clipper


window = None


def select_srt_file():
    filename, _filter = _widgets.QFileDialog.getOpenFileName(filter="*.srt")
    if filename:
        window.srtFileLineEdit.setText(filename)


def select_keywords_file():
    filename, _filter = _widgets.QFileDialog.getOpenFileName()
    if filename:
        window.keywordsFileLineEdit.setText(filename)


def select_video_input_file():
    filename, _filter = _widgets.QFileDialog.getOpenFileName(
        filter="Videos (*.mp4 *.mkv *.avi)"
    )
    if filename:
        window.videoInputFileLineEdit.setText(filename)


def scan_srt_file():
    keywords_filepath = window.keywordsFileLineEdit.text()
    if not keywords_filepath:
        _widgets.QMessageBox.critical(
            window, "No kewords file", "No keywords file selected!",
        )
        return

    with open(keywords_filepath) as keywords_file:
        keywords = {line.lower().rstrip() for line in keywords_file}

    srt_filepath = window.srtFileLineEdit.text()
    if not srt_filepath:
        _widgets.QMessageBox.critical(
            window, "No srt file", "No subtitle file (*.srt) selected!",
        )
        return
    with open(srt_filepath) as srt_file:
        matches = _scanner.parse_srt(srt_file, keywords)
        if not matches:
            _widgets.QMessageBox.info(
                window,
                "No matches",
                "No keyword matches found in the selected subtitle file (*.srt)",
            )
            return

        _widgets.QApplication.setOverrideCursor(_core.Qt.WaitCursor)
        window.scenesListWidget.clear()
        for match in matches:
            text = f"Index: {match['index']} - Keywords: {match['keywords']}"
            item = _widgets.QListWidgetItem(text)
            item.setFlags(item.flags() | _core.Qt.ItemIsUserCheckable)
            item.setCheckState(_core.Qt.Unchecked)
            window.scenesListWidget.addItem(item)
            item.setData(_core.Qt.DisplayRole, text)
            item.setData(_core.Qt.UserRole, match)
        _widgets.QApplication.restoreOverrideCursor()


def show_item_details(current, _previous):
    data = current.data(_core.Qt.UserRole)
    window.sceneSentenceTextBrowser.setText(f"{data['sentence']}")
    window.sceneKeywordsLineEdit.setText(f"{data['keywords']}")
    window.sceneTimeLineEdit.setText(f"From: {data['start']} - To: {data['end']}")


def create_clip():
    matches = []
    for idx in range(window.scenesListWidget.count()):
        item = window.scenesListWidget.item(idx)
        if not item.checkState() == _core.Qt.Checked:
            continue

        data = item.data(_core.Qt.UserRole)
        matches.append(data)

    if not matches:
        _widgets.QMessageBox.critical(
            window, "No scenes", "No scenes selected!",
        )
        return

    input_filename = window.videoInputFileLineEdit.text()
    if not input_filename:
        _widgets.QMessageBox.critical(
            window, "No video input", "No video input file selected!",
        )
        return

    override = window.overrideExistingCheckBox.isChecked()
    if os.path.exists(input_filename) and not override:
        _widgets.QMessageBox.critical(
            window,
            "Output file exists",
            (
                "The output file you have specified already exists."
                "Tick the override checkbox in the 'Video Settings' or"
                f" delete the existing file {input_filename!r}."
            ),
        )
        return

    output_filename = window.videoOutputLineEdit.text()
    _widgets.QApplication.setOverrideCursor(_core.Qt.WaitCursor)
    try:
        _clipper.create_clips(
            input_filename,
            output_filename or "output.webm",
            matches,
            audio=window.enableAudioCheckBox.isChecked(),
            subtitles=window.enableSubtitlesCheckBox.isChecked(),
            crf=window.crfSpinBox.value(),
            offset=window.clipOffsetSpinBox.value(),
            duration=window.clipDurationSpinBox.value(),
            override=override,
        )
    except Exception as exc:  # pylint: disable=broad-except
        _widgets.QMessageBox.critical(window, "Some error occurd", f"Details: {exc!s}")
    _widgets.QApplication.restoreOverrideCursor()


def run():
    # Create the Qt Application
    app = _widgets.QApplication(sys.argv)

    ui_file = _core.QFile("kektek/kektek.ui")
    if not ui_file.open(_core.QIODevice.ReadOnly):
        print("Cannot open UI file")
        sys.exit(-1)

    ui_file.open(_core.QFile.ReadOnly)

    loader = _uitools.QUiLoader()

    global window  # pylint: disable=global-statement
    window = loader.load(ui_file)

    window.srtFileToolButton.clicked.connect(select_srt_file)
    window.keywordsFileToolButton.clicked.connect(select_keywords_file)
    window.videoInputFileToolButton.clicked.connect(select_video_input_file)
    window.scanSrtFileButton.clicked.connect(scan_srt_file)
    window.createClipButton.clicked.connect(create_clip)
    window.scenesListWidget.currentItemChanged.connect(show_item_details)
    window.show()

    sys.exit(app.exec_())
