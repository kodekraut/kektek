from datetime import datetime, timedelta
import logging
import json

import click
import dateutil.parser as dtparser
import ffmpy
import srt


logger = logging.getLogger(__name__)


# ffmpeg
# -o foo                         # output file
# -crf 23                        # constent rate factor, less is better. 23 is default
# -ss $(start - interval_buffer) # time offset
# -t length                      # duration
# -vf scale=640:-1               # video filter scale to size
# -vf subtitles=<srtfile>        # video filter subtitles
# -vcodec libvpx                 # use codec
# -sn                            # disable subtitle
# -an                            # disable audio


def create_clips(
    input_filename,
    output_filename,
    scenes,
    crf,
    audio=False,
    subtitles=True,
    offset=15,
    duration=30,
    override=True,
):
    outputs = []
    for (scene_no, scene) in enumerate(scenes):
        scene_name = output_filename.replace(".webm", f"_{scene_no + 1}.webm")

        t = datetime.strptime(scene["start"], "%H:%M:%S.%f")
        time_offset = timedelta(
            hours=t.hour, minutes=t.minute, seconds=t.second
        ) - timedelta(seconds=offset)
        scene_duration = timedelta(seconds=duration)
        output_args = f"-crf {crf} -sn -vf scale=640:-1 -vcodec libvpx -ss {time_offset} -t {scene_duration}"
        if subtitles:
            output_args += f" -vf subtitles='{scene['file']}'"

        if audio:
            output_args += " -an"

        global_options=["-y" if override else "-n"]

        worker = ffmpy.FFmpeg(
            global_options=global_options,
            inputs={input_filename: None},
            outputs={scene_name: output_args},
        )
        outputs += [scene_name]
        logger.info("Scene creation command %r", worker.cmd)
        worker.run()

    input_dict = {}
    for output in outputs:
        input_dict[output] = None

    filter_string = ""
    for i, output in enumerate(outputs):
        filter_string += f"[{i}:v:0]"
        if audio:
            filter_string += f"[{i}:a:0]"

    filter_string += f"concat=n={len(outputs)}:v=1:a={int(audio)}[outv]"
    if audio:
        filter_string += "[outa] -map '[outa]'"

    filter_string += " -map '[outv]'"
    worker = ffmpy.FFmpeg(
        global_options=global_options,
        inputs=input_dict,
        outputs={
            output_filename: f"-crf {crf} -sn -vcodec libvpx -filter_complex {filter_string}"
        },
    )
    logger.info("Scene concat command %r", worker.cmd)
    worker.run()
    logger.info("Clip creation finished!", worker.cmd)


def select_scenes(matches, interactive):
    selected = []
    for scene in matches:
        scene = json.loads(scene)
        if interactive and not click.confirm(
            f"Add the following scene? \n Dialog: {scene['sentence']!r}\n Keyword(s): {scene['keywords']!r}\n Time: {scene['start']}?",
            default=True,
        ):
            continue
        selected.append(scene)
    return selected


def run(
    input_filename,
    output_filename,
    matches_file,
    crf,
    interactive,
    override,
    enable_audio,
):
    scenes = select_scenes(matches_file, interactive)
    if not scenes:
        logger.info("No scenes selected")
        return

    create_clips(
        input_filename,
        output_filename,
        scenes,
        crf,
        override=override,
        audio=enable_audio,
    )
