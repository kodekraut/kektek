#!/bin/env python
# pylint: disable=missing-function-docstring,missing-module-docstring,invalid-name,bad-continuation,too-many-locals,too-many-branches,too-many-nested-blocks


import os
import sys
import re
import logging
import json

import requests
import bs4


logging.basicConfig()
logging.root.setLevel("INFO")
logger = logging.getLogger(__name__)


EXP = re.compile(r"/show/\d")
BASE_URL = "https://www.addic7ed.com"


def show_link(tag):
    href = tag.attrs.get("href")
    return tag.name == "a" and href and EXP.match(href)


def create_show_info(link):
    href = link.attrs.get("href")
    return {
        "id": href.rpartition("/")[-1],
        "name": link.text,
        "url": f"{BASE_URL}{href}",
    }


def create_episode_info(episode):
    fields = [
        field for i, field in enumerate(episode.children) if i in [0, 1, 3, 6, 10]
    ]

    info = {
        "season": int(fields[0].text),
        "episode": int(fields[1].text),
        "language": fields[2].text,
        "status": fields[3].text,
        "url": fields[4].a.attrs["href"],
    }
    return {"id": (info["season"], info["episode"]), **info}


def create_filename(resp):
    try:
        filename = resp.headers["content-disposition"]
        filename = filename[filename.index("filename=") + 9 :]
    except (ValueError, KeyError):
        logger.warning("Failed to determine filename")
        return None

    filename = filename.replace('"', "")
    filename = filename.replace(" ", "_")
    return filename


def ensure_show_folder(base_folder, show, season):
    result = "."
    for directory in [base_folder, show["name"], season]:
        result += f"/{directory}"
        if not os.path.exists(result):
            os.mkdir(result)
    return result


def check_downloaded_shows(folder):
    show_folder = f"{folder}"
    if os.path.exists(show_folder):
        *shows, last = sorted(os.listdir(show_folder))
        if shows:
            logger.info(
                "Skipping the following files %r because srt file(s) already exist(s)",
                json.dumps(shows, indent=4),
            )
            logger.info("Redownloading %r because it might be incomplete", last)
    else:
        shows = []

    return set(shows)


def run(folder, resume):
    # Request show index page
    resp = requests.get(f"{BASE_URL}/shows.php")
    if not resp.ok:
        logger.error("Failed to download index page")
        sys.exit(-1)

    # parse html
    index_soup = bs4.BeautifulSoup(resp.content, "html.parser")

    finished_shows = check_downloaded_shows(folder) if resume else []

    # extract show links
    shows = sorted(
        [create_show_info(link) for link in index_soup.find_all(show_link)],
        key=lambda x: x["name"],
    )

    shows = [show for show in shows if show["name"] not in finished_shows]

    # Process shows
    for i, show_info in enumerate(shows):
        logger.info("Processing show Name %r (%d/%d)", show_info, i, len(shows))

        # Request specific show page
        resp = requests.get(show_info["url"])
        if not resp.ok:
            logger.warning("Failed to download process page for show %r", show_info)
            continue

        # Check if we already have srt files for the specific show
        finished_episodes = set()

        # parse show page
        show_soup = bs4.BeautifulSoup(resp.content, "html.parser")
        while True:
            episode_info = {}

            # Process show episodes
            for episode in show_soup.select("tr.epeven.completed"):
                episode_info = create_episode_info(episode)

                # ignore duplicate
                if episode_info["id"] in finished_episodes:
                    logger.debug("Already downloaded subs for %r", episode_info["id"])
                    continue

                # only consider completed and english episodes which are not "finished_episodes" yet
                if (
                    episode_info["language"].lower() == "english"
                    and episode_info["status"].lower() == "completed"
                    and episode_info["id"] not in finished_episodes
                ):
                    # Request srt file for episode
                    resp = requests.get(
                        f"{BASE_URL}/{episode_info['url']}",
                        # Setting the referer is important otherwise we get a 404
                        headers={"referer": BASE_URL},
                    )
                    if not resp.ok:
                        logger.warning(
                            "Failed to download srt file for %r", episode_info
                        )
                        continue

                    show_folder = ensure_show_folder(
                        folder, show_info, episode_info["season"]
                    )

                    # ignore subs if filename could not be determined
                    filename = create_filename(resp)
                    if not filename:
                        continue

                    # Write srt content to file
                    filename = f"{show_folder}/{filename}"
                    with open(filename, "wb") as resultfile:
                        resultfile.write(resp.content)

                    logger.info("Finished writing file: %r", filename)
                    finished_episodes.add(episode_info["id"])

            # rinse repeat until season counter - 1 is < 1
            season = episode_info.get("season", 1) - 1
            if season < 1:
                break

            # This URL is used for selecting seasons. By default the show page selects the latest
            # season.
            prev_season_url = (
                f"ajax_loadShow.php?bot=1&show={show_info['id']}&season={season}"
            )
            prev_season_url = f"{BASE_URL}/{prev_season_url}"
            # Request previous season
            resp = requests.get(prev_season_url)
            if not resp.ok:
                logger.warning(
                    "Failed to download season %d for show  %r", season, show_info
                )
                continue

            # parse content and rinse and repeat
            show_soup = bs4.BeautifulSoup(resp.content, "html.parser")
