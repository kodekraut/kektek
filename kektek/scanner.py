import os
import re
import json
import logging

import srt

logger = logging.getLogger(__name__)


def clean_up(text):
    return (
        text.replace("!", "").replace(".", "").replace(",", "").replace("?", "").lower()
    )


def parse_srt(srt_file, keywords):
    matches = {}
    for sentence in srt.parse(srt_file):
        text = sentence.content
        for keyword in keywords:
            idx = sentence.index
            if re.search(rf"(^|\s){keyword}($|\s)", clean_up(text)):
                existing = matches.get(idx)
                if existing:
                    existing["keywords"].append(keyword)
                    continue

                matches[idx] = {
                    "index": idx,
                    "file": srt_file.name,
                    "sentence": text,
                    "keywords": [keyword],
                    "start": str(sentence.start),
                    "end": str(sentence.end),
                }

    matches = sorted(matches.values(), key=lambda x: x["index"])
    return matches


def process_file(srt_file, keywords, output_path):
    show_name, episode_id, *_ = srt_file.split("-", 2)
    show_name = show_name.rpartition("/")[2]
    output_filename = show_name[:-1] + episode_id + "matches.jsonl"

    logging.info("Processing file %r", srt_file)
    with open(srt_file) as handle:
        try:
            detected_list = parse_srt(handle, keywords)
        except UnicodeError:
            logger.error("Failed to parse srt file due to unicode error")
            return

    with open(f"{output_path}/{output_filename}", "w") as output_file:
        for detected in detected_list:
            line = json.dumps(detected)
            output_file.write(line + "\n")


def process_folder(folder, keywords, output_path):
    for some_file in os.listdir(folder):
        path = os.path.join(folder, some_file)
        if os.path.splitext(some_file)[1] == ".srt":
            process_file(path, keywords, output_path)
        elif os.path.isdir(path):
            process_folder(path, keywords, output_path)


def run(srt_files, keywords_file, output_path):
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    keywords = {line.lower().rstrip() for line in keywords_file}
    for srt_file in srt_files:
        if os.path.isdir(srt_file):
            process_folder(srt_file, keywords, output_path)
        else:
            process_file(srt_file, keywords, output_path)
