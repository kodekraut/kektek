#!/bin/env python

# pylint: disable=missing-function-docstring,missing-module-docstring,invalid-name,bad-continuation

import logging

import click

import kektek.crawler as _crawler
import kektek.scanner as _scanner
import kektek.clipper as _clipper

import kektek.kektek_ui as _ui

logging.basicConfig()
logging.root.setLevel("INFO")
logger = logging.getLogger(__name__)


@click.group()
def kektek():
    """The Swiss army knife of counter porkabanda

    Est. 1834
    """


@kektek.command()
@click.option(
    "--folder",
    default="srts",
    type=click.Path(),
    help="Specifies the destination folder",
)
@click.option(
    "--resume",
    help="Do not redownload srt files if they already exist. ",
    is_flag=True,
    default=True,
)
def crawl(folder, resume):
    """ Download srt files from www.addic7ed.com"""
    _crawler.run(folder, resume)


@kektek.command()
@click.argument("srt-files", type=click.Path(), nargs=-1, required=True)
@click.option("--keywords-file", type=click.File(), required=True)
@click.option("--output-path", default="matches", type=click.Path())
def scan(srt_files, keywords_file, output_path):
    """Scan a bunch of srt files for a list of given key words"""
    _scanner.run(srt_files, keywords_file, output_path)


@click.option(
    "--input-filename",
    type=click.Path(exists=True),
    required=True,
    help="Video input file (accepted formats are .mp4/.mkv)",
)
@click.option(
    "--output-filename",
    type=click.Path(),
    default="clip",
    help="Output filename",
)
@kektek.command()
@click.option(
    "--matches-file",
    type=click.File(),
    required=True,
    help="File containing keyword matches created by 'kektek scan'",
)
@click.option(
    "--crf",
    default=23,
    help=(
        "Specifies the constant rate factor which is a value between 1-51 that defines "
        "the quality of the resulting clip). The range of the CRF scale is 4–64, where "
        "0 is lossless, 23 is the default, and 51 is worst quality possible."
    ),
)
@click.option(
    "--interactive",
    is_flag=True,
    default=False,
    help="Prompts the user to interactively select scenes from the `matches-file`.",
)
@click.option(
    "--override-existing",
    is_flag=True,
    default=False,
    help="Override output file if it already exists.",
)
@click.option(
    "--enable-audio",
    is_flag=True,
    default=False,
    help="Enable audio",
)
def clip(
    input_filename,
    output_filename,
    matches_file,
    crf,
    interactive,
    enable_audio,
    override_existing,
):
    """ Create clips from a mkv and srt file"""
    _clipper.run(
        input_filename,
        output_filename,
        matches_file,
        crf,
        interactive,
        override_existing,
        enable_audio,
    )


@kektek.command()
def gui():
    _ui.run()


if __name__ == "__main__":
    kektek()
